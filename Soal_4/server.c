#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define PORT 15050
#define BUFFER_SIZE 1024

int main() {
    int sockfd;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_len = sizeof(client_addr);
    char buffer[BUFFER_SIZE];
    FILE *fp;

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        perror("Socket creation failed");
        exit(1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);
    server_addr.sin_addr.s_addr = INADDR_ANY;

    if (bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("Socket binding failed");
        exit(1);
    }

    while (1) {
        printf("Waiting for a file...\n");

        recvfrom(sockfd, buffer, BUFFER_SIZE, 0, (struct sockaddr *)&client_addr, &client_len);
        printf("File name received: %s\n", buffer);

        fp = fopen(buffer, "rb");
        if (fp == NULL) {
            perror("File open failed");
            sendto(sockfd, "File Not Found", strlen("File Not Found"), 0, (struct sockaddr *)&client_addr, client_len);
        } else {
            while (1) {
                size_t n = fread(buffer, 1, BUFFER_SIZE, fp);
                if (n <= 0) {
                    break;
                }
                sendto(sockfd, buffer, n, 0, (struct sockaddr *)&client_addr, client_len);
            }

            fclose(fp);
            printf("File sent successfully.\n");
        }
    }

    close(sockfd);
    return 0;
}
