#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define baris 2
#define kolom 2
#define MIN_RANGE1 1
#define MAX_RANGE1 4
#define MIN_RANGE2 1
#define MAX_RANGE2 5

void perkalian(int matrix1[baris][kolom], int matrix2[baris][kolom], int result[baris][kolom]){
    for (int i=0; i<baris; i++){
        for (int j = 0; j < kolom; j++){
            result[i][j] = 0;
            for (int k = 0; k < kolom; k++){
                result[i][j] += matrix1 [i][k] * matrix2[k][j];
            }
        }
    }
}

void pengurangan(int matrix[baris][kolom]){
    for (int i=0; i<baris; i++){
        for (int j=0; j<kolom; j++){
            matrix[i][j] -= 1;
        }
    }
}

void showMatrix(int matrix[baris][kolom], char* label){
    printf("%s:\n", label);
    for (int i=0; i<baris; i++){
        for (int j=0; j<kolom; j++){
            printf("%3d", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}


int main(){
    int matrix1[baris][kolom];
    int matrix2[baris][kolom];
    int result[baris][kolom];

    srand(time(0));
    for (int i=0; i<baris; i++){
        for (int j=0; j<kolom; j++){
            matrix1[i][j] = (rand() % MAX_RANGE1) + MIN_RANGE1;
            matrix2[i][j] = (rand() % MAX_RANGE2) + MIN_RANGE2;
        }
    }

 showMatrix(matrix1, "Matriks Pertama");
 showMatrix(matrix2, "Matriks Kedua");

    perkalian(matrix1, matrix2, result);

 showMatrix(result, "Hasil Perkalian Matriks");

    pengurangan(result);

 showMatrix(result, "Hasil pengurangan -1");

    key_t key = 5678;
        int (*sharedMatrix)[kolom];

        int shmid = shmget(key, sizeof(int[baris][kolom]), IPC_CREAT | 0666);
        sharedMatrix = shmat(shmid, NULL, 0);

    for(int i = 0; i<baris; i++){
        for(int j = 0; j<kolom; j++){
            sharedMatrix[i][j] = result[i][j];
        }
    }

    shmdt(sharedMatrix);

    return 0;

}