# sisop-praktikum-modul-3-2023-MH-IT20
Laporan pengerjaan soal shift modul 3 Praktikum Sistem Operasi 2023 Kelompok IT20

## Anggota Kelompok:
| N0. | Nama Anggota | NRP |
|-----| ----------- | ----------- |
| 1. | M. Januar Eko Wicaksono | 5027221006 |
| 2. | Rizki Ramadhani | 5027221013 |
| 3. |Khansa Adia Rahma | 5027221071 | 

## Soal 1

### Study case soal 1
---
Epul memiliki seorang adik SMA bernama Azka. Azka bersekolah di Thursina Malang. Di sekolah, Azka diperkenalkan oleh adanya kata matriks. Karena Azka baru pertama kali mendengar kata matriks, Azka meminta tolong kepada kakaknya Epul untuk belajar matriks.  Karena Epul adalah seorang kakak yang baik, Epul memiliki ide untuk membuat program yang bisa membantu adik tercintanya, tetapi dia masih bingung untuk membuatnya. Karena Epul adalah bagian dari IT05 dan praktikan sisop, sebagai warga IT05 yang baik, bantu Epul mengerjakan programnya dengan ketentuan sebagai berikut :

### Problem
---
a Membuat program C dengan nama belajar.c, yang berisi program untuk melakukan perkalian matriks. Agar ukuran matriks bervariasi, maka ukuran matriks pertama adalah [nomor_kelompok]×2 dan matriks kedua 2×[nomor_kelompok]. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-4 (inklusif), dan rentang pada matriks kedua adalah 1-5 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

b. Karena Epul merasa ada yang kurang, Epul meminta agar hasil dari perkalian tersebut dikurang 1 di setiap matriks.

c. Karena Epul baru belajar modul 3, Epul ngide ingin meminta agar menerapkan konsep shared memory. Buatlah program C kedua dengan nama yang.c. Program ini akan mengambil variabel hasil pengurangan dari perkalian matriks dari program belajar.c (program sebelumnya). Hasil dari pengurangan perkalian matriks tersebut dilakukan transpose matriks dan diperlihatkan hasilnya. 
(Catatan: wajib menerapkan konsep shared memory)

d. Setelah ditampilkan, Epul pengen adiknya belajar lebih, sehingga untuk setiap angka dari transpose matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

**Contoh:** 

matriks: 
```c
1	2  	3

2	2	4
```
maka: 
```c
1	4  	6

4	4	24
```
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial).

e. Epul penasaran mengapa dibuat thread dan multithreading pada program sebelumnya, dengan demikian dibuatlah program C ketiga dengan nama rajin.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada yang.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

Catatan:
- Key memory diambil berdasarkan program dari belajar.c sehingga program belajar.c tidak perlu menghasilkan key memory (value). Dengan demikian, pada program yang.c dan rajin.c hanya perlu mengambil key memory dari belajar.c
- Untuk kelompok 1 - 9, 11 - 19, 21 menggunakan  digit akhir saja.
	Contoh : 19 -> 9
- Untuk kelompok 10 dan 20 menambahkan digit awal dan akhir.
			Contoh : 20 - > 2

### Solution
---
[Source Code](./Soal_1)

A. Belajar.c
Pertama-tama membuat file Bernama belajar.c, dimana pada program ini akan menghasilkan 2 matriks dengan angka acak dalam rentang matriks pertama yaitu 1-4 dan rentang matriks kedua 1-5. Selanjutnya kedua matriks akan dikalikan, dan dikurangi 1 pada setiap elemen hasil perkalian sebelumnya. 

1. Menggunakan beberapa library pada belajar.c yaitu: 
- stdio.h: Library ini digunakan untuk fungsi input-output standar seperti printf dan scanf.
- stdlib.h: Library ini mengandung fungsi-fungsi umum seperti alokasi memori dinamis dengan malloc dan free, penghasil angka acak dengan rand, serta konversi string ke angka dengan atoi.
- time.h: Library ini digunakan untuk mengakses fungsi-fungsi terkait waktu, seperti time, yang digunakan untuk menghasilkan seed untuk fungsi penghasil angka acak.
- sys/ipc.h: Library ini menyediakan fungsi-fungsi untuk mengakses sistem V interprocess communication (IPC), termasuk shared memory.
- sys/shm.h: Library ini berisi fungsi-fungsi yang digunakan untuk mengelola shared memory, seperti shmget dan shmat, yang digunakan dalam program ini.

2. Menggunakan fungsi rand() untuk membuat 2 matriks dengan angka acak dan rentang yang sudah ditentukan.

```c
srand(time(0));
    for (int i=0; i<baris; i++){
        for (int j=0; j<kolom; j++){
            matrix1[i][j] = (rand() % MAX_RANGE1) + MIN_RANGE1;
            matrix2[i][j] = (rand() % MAX_RANGE2) + MIN_RANGE2;
        }
    }
```
3.	Setelah 2 matriks dibuat, dilakukan perkalian matriks menggunakan fungsi perkalian(). Pada fungsi ini setiap elemen matriks pertama dan kedua akan dikalikan dan menjumlahkan hasilnya.
```c
void perkalian(int matrix1[baris][kolom], int matrix2[baris][kolom], int result[baris][kolom]){
    for (int i=0; i<baris; i++){
        for (int j = 0; j < kolom; j++){
            result[i][j] = 0;
            for (int k = 0; k < kolom; k++){
                result[i][j] += matrix1 [i][k] * matrix2[k][j];
            }
        }
    }
}
```

4. Setelah perkalian matriks dilakukan, maka akan dilakukan pengurangan 1 untuk setiap elemen dari hasil perkalian matriks menggunakan fungsi pengurangan().
```c
void pengurangan(int matrix[baris][kolom]){
    for (int i=0; i<baris; i++){
        for (int j=0; j<kolom; j++){
            matrix[i][j] -= 1;
        }
    }
}
```
5. Setelah pengurangan matriks dilakukan, hasilnya akan ditampilkan menggunakan
fungsi showMatrix().
```c
void showMatrix(int matrix[baris][kolom], char* label){
    printf("%s:\n", label);
    for (int i=0; i<baris; i++){
        for (int j=0; j<kolom; j++){
            printf("%3d", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
```
6. Selanjutnya menggunakan konsep shared memory untuk berbagi hasil matriks
pengurangan dengan program file lain. 
```c
key_t key = 5678;
        int (*sharedMatrix)[kolom];

        int shmid = shmget(key, sizeof(int[baris][kolom]), IPC_CREAT | 0666);
        sharedMatrix = shmat(shmid, NULL, 0);

    for(int i = 0; i<baris; i++){
        for(int j = 0; j<kolom; j++){
            sharedMatrix[i][j] = result[i][j];
        }
    }

    shmdt(sharedMatrix);
```
Program ini menggunakan key 5678 untuk mengakses shared memory. Matriks hasil pengurangan disimpan dalam shared memory menggunakan pointer sharedMatrix dan kemudian diakhiri dengan melepaskan shared memory menggunakan shmdt().

B. Yang.c
Pada program file yang.c ini akan melakukan transpose matriks berdasarkan matriks hasil pengurangan dari belajar.c, selanjutnya akan dilakukan perhitungan factorial dari hasil transpose tersebut. Penggunaan konsep shared memory dan thread juga terdapat dalam yang.c untuk melakukan perhitungan.

1. Pada yang.c ini digunakan beberapa library, yaitu:
- stdio.h: Library standar untuk fungsi input-output
- stdlib.h: Library standar yang berisi fungsi-fungsi umum
- sys/ipc.h: Library untuk mengakses sistem V interprocess communication (IPC)
- sys/shm.h: Library untuk mengelola shared memory
- sys/time.h: Library untuk mengakses waktu
- sys/types.h: Library untuk tipe data khusus seperti pthread_t

2.	Transpose Matriks
Menggunakan fungsi `transposeMatrix` untuk melakukan operasi transpose pada matriks.
```c
void transposeMatrix(int matrix[baris][kolom], int transposed[kolom][baris]) {
    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < kolom; j++) {
            transposed[j][i] = matrix[i][j];
        }
    }
}
``` 
Pada fungsi ini menerima 2 parameter matrix sebagai matriks input dan transposed sebagai matriks hasil transpose. 

3.	Perhitungan Faktorial
Menggunakan fungsi `factorial` untuk menghitung factorial dari suatu bilangan. 
```c
unsigned long long factorial(int n) {
    if (n <= 1) {
        return 1;
    } else {
        unsigned long long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}
```
Pada fungsi ini menerima bilangan n sebagai parameter. Jika n kurang dari atau sama dengan 1, maka fungsi akan mengembalikan nilai 1. Jika tidak, fungsi akan menghitung factorial dengan loop for.

4. Fungsi Thread
Menggunkaan fungsi`kalkuFactorial` yang dimana menerapkan konsep thread pada fungsi ini. Fungsi ini juga mengakses shared memory untuk mendapatkan matriks transpose, kemudian dilakukan perhitungan factorial pada elemen matriks transposenya.
```c
void* kalkuFactorial(void* arg) {
    MatrixIndex* indeks = (MatrixIndex*)arg;
    int row = indeks->row;
    int col = indeks->col;

    int matrix[kolom][baris];
    key_t key = 5678;

    int shmid = shmget(key, sizeof(int[baris][kolom]), 0666);
    int (*sharedMatrix)[kolom] = shmat(shmid, (void*)0, 0);

    int transposedMatrix[kolom][baris];
    transposeMatrix(sharedMatrix, transposedMatrix);

    unsigned long long result = factorial(transposedMatrix[col][row]);

    shmdt(sharedMatrix);

    unsigned long long* result_ptr = (unsigned long long*)malloc(sizeof(unsigned long long));
    *result_ptr = result;

    return result_ptr;
}
```
Pada fungsi ini menerima argument berupa pointer ke struct MatrixIndex, berisi indeks baris dan kolom dari matriks. Dalam fungsi ini juga menggunakan shmget untuk mengakses shared memory dan shmat mendapatkan matriks transposenya. Untuk melakukan operasi transpose pada matriks makan dipanggil fungsi transposeMatrix. Menggunakan fungsi factorial untuk menghitung factorial dari elemen matriks transposenya. Selanjutnya, menggunakan shmdt untuk melepas shared memory. Untuk pengalokasian memori dan menyimpan hasil factorial digunakan malloc. 

5. Main Function
Pada fungsi main, program mengakses shared memory, mendapatkan matriks transpose, dan menampilkan matriks transposenya. Selanjutnya, membuat thread-thread untuk menghitung faktorial pada setiap elemen matriks transposenya. Lanjut, mengumpulkan hasil perhitungan faktorial dari setiap thread dan menyimpannya dalam matriks `factorialMatrix`. Perhitungan waktu total untuk menjalankan program juga terdapat pada fungsi ini. Terakhir, menampilkan matriks `factorialMatrix` dan total time yang dihasilkan.
```c
int main() {
    key_t key = 5678;

    int shmid = shmget(key, sizeof(int[baris][kolom]), 0666);

    int (*sharedMatrix)[kolom] = shmat(shmid, (void*)0, 0);

    int transposedMatrix[kolom][baris];
    transposeMatrix(sharedMatrix, transposedMatrix);

    struct timeval start, end;
    gettimeofday(&start, NULL);

    printf("Transposed Matrix:\n");
    for (int i = 0; i < kolom; i++) {
        for (int j = 0; j < baris; j++) {
            printf("%12d", transposedMatrix[i][j]);
        }
        printf("\n");
    }

    pthread_t threads[baris][kolom];
    MatrixIndex indeks[baris][kolom];

    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < kolom; j++) {
            indeks[i][j].row = i;
            indeks[i][j].col = j;

            pthread_create(&threads[i][j], NULL, kalkuFactorial, &indeks[i][j]);
        }
    }

    unsigned long long factorialMatrix[baris][kolom];
    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < kolom; j++) {
            unsigned long long* result;
            pthread_join(threads[i][j], (void**)&result);
            factorialMatrix[j][i] = *result;
            free(result);
        }
    }

     gettimeofday(&end, NULL);
     double totalTime = (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000000.0;

    printf("Factorial Matrix:\n");
    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < baris; j++) {
            printf("%25llu", factorialMatrix[i][j]);
        }
        printf("\n");
    }

    printf("Total Time: %f sec\n", totalTime);

    shmdt(sharedMatrix);

    return 0;
}
```
C. Rajin.c
Pada file ke 3 yaitu rajin.c memuat kegunaan yang sama seperti yang.c yaitu melakukan operasi transpose pada matriks yang didapatkan dari shared memory, dan menghitung factorial dari setiap elemen matriks hasil transpose. 

1. Transpose Matriks
Menggunakan fungsi `transposeMatrix` untuk melakukan operasi transpose pada matriks. Matriks input diperoleh dari shared memory, dan matriks hasil transpose akan disimpan pada matriks `transposedMatrix`.
```c
void transposeMatrix(int matrix[baris][kolom], int transposed[kolom][baris]) {
    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < kolom; j++) {
            transposed[j][i] = matrix[i][j];
        }
    }
}
```
2. Factorial
Menggunakan fungsi factorial untuk menghitung factorial dari setiap elemen matriks hasil transpose dan disimpan pada matriks factorialMatrix. 
```c
unsigned long long factorial(int n) {
    if (n <= 1) {
        return 1;
    } else {
        unsigned long long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}
```
3. Main Function
Pada fungsi main, program mengakses shared memory. Selanjutnya program melakukan operasi dan menampilkan transpose matriks menggunakan fungsi transposedMatrix. Program juga menghitung dan menampilkan hasil factorial menggunakan fungsi factorial. Terakhir, program menghitung dan menampilkan total waktu eksekusi menggunakan fungsi gettimeofday.
```c
int main() {
    key_t key = 5678;

    int shmid = shmget(key, sizeof(int[baris][kolom]), 0666);

    int (*sharedMatrix)[kolom] = shmat(shmid, (void*)0, 0);

    int transposedMatrix[kolom][baris];
    transposeMatrix(sharedMatrix, transposedMatrix);

    struct timeval start, end;
    gettimeofday(&start, NULL);

    printf("Transposed Matrix:\n");
    for (int i = 0; i < kolom; i++) {
        for (int j = 0; j < baris; j++) {
            printf("%12d", transposedMatrix[i][j]);
        }
        printf("\n");
    }

    unsigned long long factorialMatrix[baris][kolom];
    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < kolom; j++) {
            factorialMatrix[i][j] = factorial(transposedMatrix[i][j]);
        }
    }

    gettimeofday(&end, NULL);
    double totalTime = (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000000.0;

    printf("Factorial Matrix:\n");
    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < baris; j++) {
            printf("%25llu", factorialMatrix[i][j]);
        }
        printf("\n");
    }

    printf("Total Time: %f sec\n", totalTime);

    shmdt(sharedMatrix);

    return 0;
}
```

### Kendala
---
1. Tidak ada kendala apapun.

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Tidak ada, pada saat demo sudah lancar dan tidak ada tambahan dari aslab penguji

### Hasil
---

1. Hasil Run belajar.c `belajar.c`

![hasil File 'belajar.c'](img/Soal_1/1.png)

2. Hasil Run yang.c `yang.c`

![hasil File 'yang.c'](img/Soal_1/2.png)

3. Hasil Run rajin.c `rajin.c`

![hasil File 'rajin.c'](img/Soal_1/3.png)


## Soal 2

### Study case soal 2
---
Messi the only GOAT sedang gabut karena Inter Miami Gagal masuk Playoff. Messi menghabiskan waktu dengan mendengar lagu. Karena gabut yang tergabut gabut, Messi penasaran berapa banyak jumlah kata tertentu yang muncul pada lagu tersebut. Messi mencoba ngoding untuk mencari jawaban dari kegabutannya. Pertama-tama dia mengumpulkan beberapa lirik lagu favorit yang disimpan di file. Beberapa saat setelah mencoba ternyata ngoding tidak semudah itu, Messipun meminta tolong kepada Ronaldo, tetapi karena Ronaldo sibuk main di liga oli, Ronaldopun meminta tolong kepadamu untuk dibuatkan kode dari program Messi dengan ketentuan sebagai berikut : 

(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).
### Problem
---
a. Messi akan menjalankan program 8ballondors.c pada parent process lalu program akan membaca file dan menghapus karakter-karakter yang bukan huruf pada file dan output file dari hasil process itu dengan nama thebeatles.txt dan akan menghitung jumlah frekuensi yang di inputkan oleh user.

b. Karena gabutnya, Messi ingin program dapat mencari jumlah frekuensi kemunculan sebuah huruf dari file. Maka, pada child process lakukan perhitungan jumlah frekuensi kemunculan sebuah huruf dari output file yang akan diinputkan (huruf) user, dan Karena terdapat dua perhitungan, maka pada program buatlah argumen untuk menjalankan program : 
Kata	: ./8ballondors -kata
Huruf	: ./8ballondors -huruf

Hasil dari perhitungan  jumlah frekuensi kemunculan sebuah kata dan  jumlah frekuensi kemunculan sebuah huruf dikirim ke parent process.
Messi ingin agar setiap kata atau huruf dicatat dalam sebuah log yang diberi nama frekuensi.log. Pada parent process, lakukan pembuatan file log berdasarkan data yang dikirim dari child process. 
Format: [date] [type] [message]
Type: KATA, HURUF
Ex:
[24/10/23 01:05:48] [KATA] Kata 'yang' muncul sebanyak 10 kali dalam file 'thebeatles.txt'
[24/10/23 01:04:29] [HURUF] Huruf 'a' muncul sebanyak 396 kali dalam file 'thebeatles.txt'
Perhitungan jumlah frekuensi kemunculan kata atau huruf  menggunakan Case-Sensitive Search. 

### Solution
---
a. Pertama membuat file 8ballondors.c yaitu program akan meminta input yang di berikan oleh user, dimana program tersebut bertugas untuk membaca dan mengahpus file karakter-karakter yang bukan huruf pada file.
Berikut adalah program 8ballondors.c yang saya gunakan:
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <ctype.h>




void logMessage(char *type, char *message) {
    time_t current_time;
    struct tm *local_time;

    time(&current_time);
    local_time = localtime(&current_time);


    printf("[%02d/%02d/%02d %02d:%02d:%02d] [%s] %s\n",
           local_time->tm_mday, local_time->tm_mon + 1, local_time->tm_year % 100,
           local_time->tm_hour, local_time->tm_min, local_time->tm_sec,
           type, message);


    FILE *log_file = fopen("frekuensi.log", "a");
    if (log_file != NULL) {
        fprintf(log_file, "[%02d/%02d/%02d %02d:%02d:%02d] [%s] %s\n",
                local_time->tm_mday, local_time->tm_mon + 1, local_time->tm_year % 100,
                local_time->tm_hour, local_time->tm_min, local_time->tm_sec,
                type, message);
        fclose(log_file);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) {
        fprintf(stderr, "Usage: %s <-kata|-huruf>\n", argv[0]);
        return 1;
    }

    char *option = argv[1];
    int isWordCount = strcmp(option, "-kata") == 0;

    int fd[2];
    if (pipe(fd) == -1) {
        perror("Pipe Failed");
        return 1;
    }

    pid_t p = fork();

    if (p < 0) {
        perror("fork Failed");
        return 1;
    }

    if (p > 0) {
        close(fd[0]);

        char input_str[1000];
        printf("Enter lirik lagu: ");
        fgets(input_str, sizeof(input_str), stdin);


        FILE *output_file = fopen("thebeatles.txt", "w");
        if (output_file == NULL) {
            perror("Failed to open output file");
            return 1;
        }
        for (int i = 0; i < strlen(input_str); i++) {
            if (isalpha(input_str[i]) || input_str[i] == ' ') {
                fputc(input_str[i], output_file);
            }
        }
        fclose(output_file);

        write(fd[1], input_str, strlen(input_str) + 1);
        close(fd[1]);

        wait(NULL);

        close(fd[0]);
    } else {
        close(fd[1]);

        char input_str[1000];
        read(fd[0], input_str, sizeof(input_str));
        close(fd[0]);

        if (isWordCount) {

            char result_str[100];
            char *word = strtok(input_str, " \t\n\r");
            int count = 0;

            while (word != NULL) {
                if (strcmp(word, "dunia") == 0) {
                    count++;
                }
                word = strtok(NULL, " \t\n\r");
            }

            snprintf(result_str, sizeof(result_str), "Kata 'dunia' muncul sebanyak %d kali dalam lirik lagu", count);
            logMessage("KATA", result_str);
        } else {

            char result_str[100];
            char *character = input_str;
            int count = 0;

            while (*character != '\0') {
                if (isalpha(*character) && *character == 't') {
                    count++;
                }
                character++;
            }

            snprintf(result_str, sizeof(result_str), "Huruf 't' muncul sebanyak %d kali dalam lirik lagu", count);
            logMessage("HURUF", result_str);
        }

        close(fd[1]);
        exit(0);
    }

    return 0;
}
```

b. Setelah membuat file 8ballondors.c saya mengcompile program dengan menggunakan command "make" , tentu sebelumnya saya sudah membuat file make agar saya dapat menjalankan perintah make untuk mengcompile.

c. Menjalankan program dengan salah satu argumen berikut :
Kata	: ./8ballondors -kata
Huruf	: ./8ballondors -huruf

d. Maka program akan memberikan hasil dari perhitungan  jumlah frekuensi kemunculan sebuah kata dan  jumlah frekuensi kemunculan sebuah huruf dikirim ke parent process.

e. Program juga akan file frekuensi.log dan thebeatless.txt pada satu folder dengan 8ballondors.c serta liriklagu.txt yang saya beri nama foldernya yaitu praklagu.

### Kendala
---
1. Salah konsep

konsep yang benar 
(program meminta input user lalu membaca file liriklagu.txt)

konsep saya 
(didalam program 8ballondors saya sudah memberikan perintah untuk mencari kata gunung dan huruf t lalu program akan meminta input berupa lirik lagu nya dan akan mencari kata atau huruf yang sudah sesuai di perintahkan pada program)

### Revisi
---
Mengubah sedikit program yang awalnya didalam program 8ballondors saya sudah memberikan perintah untuk mencari kata gunung dan huruf t lalu program akan meminta input berupa lirik lagu nya dan akan mencari kata atau huruf yang sudah sesuai di perintahkan pada program menjadi program meminta input user lalu membaca file liriklagu.txt dan ketentuan yang ada pada soal.

### Hasil

1. Isi dari File `8ballondors.c`

![Isi File '8ballondors.c'](img/Soal_2/isiFile.c.png)

2. Isi dari File `frekuensi.log`

![Isi File 'frekuensi.log'](img/Soal_2/log.png)

3. Isi dari File `thebeatles.txt`

![Isi File 'thebeatles.txt'](img/Soal_2/thebeatles.png)

## Soal 3

### Study case soal 3
---
Christopher adalah seorang praktikan sisop, dia mendapat tugas dari pak Nolan untuk membuat komunikasi antar proses dengan menerapkan konsep message queue. Pak Nolan memberikan kredensial list  users yang harus masuk ke dalam program yang akan dibuat. Lebih lanjutnya pak Nolan memberikan instruksi tambahan sebagai berikut : 

### Problem
---
1. Bantulah Christopher untuk membuat program tersebut, dengan menerapkan konsep message queue(wajib) maka buatlah 2  program, sender.c sebagai pengirim dan receiver.c sebagai penerima. Dalam hal ini, sender hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh receiver.
Struktur foldernya akan menjadi seperti berikut:
```c
- soal3
    - users
    - receiver.c
    - sender.c
```

b. Ternyata list kredensial yang diberikan Pak Nolan semua passwordnya terenkripsi dengan menggunakan base64. Untuk mengetahui kredensial yang valid maka Sender pertama kali akan mengirimkan perintah CREDS kemudian sistem receiver akan melakukan decrypt/decode/konversi pada file users, lalu menampilkannya pada receiver. Hasilnya akan menjadi seperti berikut :
```c
./receiver
Username: Mayuri, Password: TuTuRuuu
Username: Onodera, Password: K0sak!
Username: Johan, Password: L!3b3rt
Username: Seki, Password: Yuk!n3
Username: Ayanokouji, Password: K!yot4kA
…..
```

c. Setelah mengetahui list kredensial, bantulah christopher untuk membuat proses autentikasi berdasarkan list kredensial yang telah disediakan. Proses autentikasi dilakukan dengan menggunakan perintah AUTH: username password kemudian jika proses autentikasi valid dan berhasil maka akan menampilkan Authentication successful . Authentication failed jika gagal.

d. Setelah berhasil membuat proses autentikasi, buatlah proses transfer file. Transfer file dilakukan dari direktori Sender dan dikirim ke direktori Receiver . Proses transfer dilakukan dengan menggunakan perintah TRANSFER filename . Struktur direktorinya akan menjadi seperti berikut:
```c
- soal3
    - Receiver
    - Sender
    - receiver.c
    - sender.c
```

e. Karena takut memorinya penuh, Christopher memberi status size(kb) pada setiap pengiriman file yang berhasil.

f. Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan **"UNKNOWN COMMAND"**.
**Catatan:**
- Dilarang untuk overwrite file users secara permanen
- Ketika proses autentikasi gagal program receiver akan mengirim status _Authentication failed_ dan program langsung keluar
- Sebelum melakukan transfer file sender harus login (melalui proses auth) terlebih dahulu
- Buat transfer file agar tidak duplicate dengan file yang memang sudah ada atau sudah pernah dikirim.


### Solution 
---
[Source Code](./Soal_3/)

1. Source Code Receiver.c.
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>


struct msg_buffer 
{
    long msg_type;
    char msg_text[100];
};

//function buat transfer file and cek file size
int transfer_file(const char *filename) 
{
    
    char src_path[256];
    char dest_path[256];
    snprintf(src_path, sizeof(src_path), "Sender/%s", filename);
    snprintf(dest_path, sizeof(dest_path), "Receiver/%s", filename);

    //rb buat read
    FILE *src_file = fopen(src_path, "rb");
    if (src_file == NULL) 
    {
        perror("Error opening source file");
        return -1;
    }

    FILE *dest_file = fopen(dest_path, "wb");
    if (dest_file == NULL) 
    {
        perror("Error opening destination file");
        fclose(src_file);
        return -1;
    }

    //transfer data n calculate size
    char buffer[1024];
    size_t bytes_read;
    size_t total_size = 0;

    while ((bytes_read = fread(buffer, 1, sizeof(buffer), src_file)) > 0) {
        fwrite(buffer, 1, bytes_read, dest_file);
        total_size += bytes_read;
    }

    //close files
    fclose(src_file);
    fclose(dest_file);

    //display file size
    double filesize = total_size / 1024.0;
    printf("File '%s' transferred successfully. Size: %.2f KB\n", filename, filesize);

    return 0;
}



//DECODE BASE64
int base64_decode(const char *input, unsigned char **output) 
{
    BIO *b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    BIO *bmem = BIO_new_mem_buf(input, -1);
    bmem = BIO_push(b64, bmem);

    *output = (unsigned char *)malloc(strlen(input));
    if (*output == NULL) 
    {
        return -1;
    }

    int length = BIO_read(bmem, *output, strlen(input));
    BIO_free_all(bmem);

    return length;
}

//FUNCTION AUTH
int authenticate_user(const char *username, const char *password) 
{
    //OPEN AND READ
    FILE *file = fopen("users/users.txt", "r");

    char line[256];
    int decoded_password_failed = 0;

    while (fgets(line, sizeof(line), file)) 
    {
        char *stored_username = strtok(line, ":");
        char *encoded_password = strtok(NULL, ":");

        if (stored_username != NULL && encoded_password != NULL) 
        {
            // Decode
            unsigned char *decoded_password = NULL;
            int decoded_length = base64_decode(encoded_password, &decoded_password);

            if (decoded_length > 0) 
            {
                //Compare string
                if (strcmp(username, stored_username) == 0 && strcmp(password, (char *)decoded_password) == 0) 
                {
                    free(decoded_password);
                    fclose(file);
                    return 1; //1=auth sukses
                }
                free(decoded_password);
            } 
            else 
            {
                decoded_password_failed = 1;
            }
        }
    }

    fclose(file);

    if (decoded_password_failed) 
    {
        return -1; //if auth failed
    }

    return 0; //exit
}

//MAIN
// ...

int main() 
{
    key_t key;
    int msg_id;
    int exit_code = 0;


    key = ftok("receiver.c", 65);
    if (key == -1) 
    {
        perror("ftok");
        exit(1);
    }

    // message queue
    msg_id = msgget(key, 0666 | IPC_CREAT);
    if (msg_id == -1) 
    {
        perror("msgget");
        exit(1);
    }

    struct msg_buffer message;

    while (1) {
        //receive dari sender
        msgrcv(msg_id, &message, sizeof(message), 1, 0);

        if (strcmp(message.msg_text, "CREDS") == 0) 
        {
            //open and read users.txt
            FILE *file = fopen("users/users.txt", "r");
            if (file == NULL) 
            {
                perror("Error opening users/users.txt");
                exit_code = 1;
                break; //Exit the loop on error
            }

            char line[256];

            while (fgets(line, sizeof(line), file)) 
            {
                char *username = strtok(line, ":");
                char *encoded_password = strtok(NULL, ":");

                if (username != NULL && encoded_password != NULL) 
                {
                    //Decode the base64
                    unsigned char *decoded_password = NULL;
                    int decoded_length = base64_decode(encoded_password, &decoded_password);

                    if (decoded_length > 0) 
                    {
                        //display username password
                        printf("Username: %s, Password: %s\n", username, (char *)decoded_password);
                        free(decoded_password);
                    } 
                    else 
                    {
                        printf("Username: %s, Failed to decode password.\n", username);
                    }
                }
            }

            fclose(file);
        } 
        
        else if (strncmp(message.msg_text, "AUTH:", 5) == 0) 
        {
            
            char *command = message.msg_text + 5;
            char *username = strtok(command, " ");
            char *password = strtok(NULL, " ");

            if (username != NULL && password != NULL) 
            {
                int auth_result = authenticate_user(username, password);
                if (auth_result == 1) 
                {
                    printf("Authentication successful\n");
                } 
                else 
                {
                    printf("Authentication failed\n");
                    exit_code = 1;
                    break; //exit loop kalau auth failed
                }
            } 
            else 
            {
                printf("Invalid AUTH command format\n");
            }
        }

        else if (strncmp(message.msg_text, "TRANSFER ", 9) == 0) 
        {
            const char *filename = message.msg_text + 9;

            int transfer_result = transfer_file(filename);

            if (transfer_result == 0) 
            {

            } 
            else 
            {
                printf("File transfer failed.\n");
                exit_code = 1;
                break;
            }
        }
        else 
        {
            printf("UNKNOWN COMMAND\n");
        }
    }

    //remove the message queue
    msgctl(msg_id, IPC_RMID, NULL);

    return exit_code;
}
```
a. Pendefinisian Struktur msg_buffer:
- Struktur msg_buffer digunakan untuk menyimpan pesan yang dikirimkan melalui sistem pesan. Ini memiliki dua anggota: msg_type (tipe pesan) dan msg_text (isi pesan). Tipe pesan digunakan untuk mengidentifikasi pesan.

b. Fungsi transfer_file:
- Fungsi ini menerima nama file sebagai argumen, membuka file sumber, membaca isinya, dan menyalinnya ke file tujuan. Ini juga menghitung ukuran file yang berhasil ditransfer.

c. Fungsi base64_decode:
- Fungsi ini digunakan untuk mendekode string yang telah dienkripsi dalam format Base64. Hasil dekripsi disimpan dalam bentuk byte yang diarahkan ke alamat yang ditentukan oleh pointer output.

d. Fungsi authenticate_user:
- Fungsi ini digunakan untuk memeriksa autentikasi pengguna. Ini membuka file "users.txt" yang berisi informasi pengguna (nama pengguna dan kata sandi yang dienkripsi dalam format Base64). Fungsi ini memeriksa apakah informasi otentikasi yang diberikan oleh pengguna sesuai dengan informasi yang ada dalam file "users.txt".

e. Fungsi main:

- Fungsi utama dari program. Ini berisi logika utama program yang melibatkan komunikasi melalui sistem pesan, autentikasi pengguna, dan transfer file.
- Program ini menggunakan kunci (key) yang dibuat dengan ftok untuk mendapatkan ID pesan dan mengakses sistem pesan.
- Program berjalan dalam loop tak terbatas (while (1)) dan menerima pesan dari pengirim melalui sistem pesan.
- Program mengenali beberapa jenis pesan, seperti "CREDS" untuk mengambil dan menampilkan informasi pengguna dari file "users.txt", "AUTH:" untuk melakukan autentikasi pengguna, dan "TRANSFER " untuk mentransfer file dari sender ke receiver.
- Jika pesan yang diterima tidak cocok dengan jenis pesan yang diharapkan, program akan mencetak "UNKNOWN COMMAND."
- Program juga menghapus antrian pesan (msg_id) saat selesai dijalankan.

Perlu dicatat bahwa program ini tidak memiliki keselamatan yang cukup dalam hal autentikasi pengguna karena kata sandi hanya dienkripsi dalam format Base64, yang dapat dengan mudah dipecahkan. Selain itu, kode ini hanya menangani operasi dasar dan tidak memiliki fitur keamanan yang kuat. Keamanan sistem autentikasi yang kuat memerlukan pendekatan yang jauh lebih canggih.
- Setelah itu save dan jalankan 'genshin.sh' dengan command dibawah ini:

2. Source code sender.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>

#include <dirent.h>

struct msg_buffer 
{
    long msg_type;
    char msg_text[100];
};

//buat list transferred file
struct TransferredFile 
{
    char filename[100];
    struct TransferredFile* next;
};

struct TransferredFile* transferredFiles = NULL;

//cek if file udh di transfer
int isFileTransferred(const char* filename) 
{
    struct TransferredFile* current = transferredFiles;
    while (current != NULL) 
    {
        if (strcmp(current->filename, filename) == 0) 
        {
            return 1; //1=sudah
        }
        current = current->next;
    }
    return 0; //0=belum
}

//add file ke list transferredfile
void addTransferredFile(const char* filename) 
{
    struct TransferredFile* newFile = (struct TransferredFile*)malloc(sizeof(struct TransferredFile));
    strncpy(newFile->filename, filename, sizeof(newFile->filename) - 1);
    newFile->next = transferredFiles;
    transferredFiles = newFile;
}

//check apakah file udh ada di folder
int isFileInReceiver(const char* filename) 
{
    DIR* dir;
    struct dirent* entry;
    dir = opendir("Receiver");

    if (dir == NULL) 
    {
        perror("opendir");
        exit(1);

    }

    while ((entry = readdir(dir))) 
    {
        if (entry->d_type == DT_REG) 
        {
            if (strcmp(entry->d_name, filename) == 0) 
            {
                closedir(dir);
                return 1; //1=ada
            }
        }
    }
    closedir(dir);
    return 0; //0=gaada
}

int main() 
{
    key_t key;
    int msg_id;
    int authenticated = 0;  //track sudah di auth blm

    key = ftok("receiver.c", 65);
    if (key == -1) 
    {
        perror("ftok");
        exit(1);
    }

    //message queue
    msg_id = msgget(key, 0666 | IPC_CREAT);
    if (msg_id == -1) 
    {
        perror("msgget");
        exit(1);

    }

    char input[100];
    struct msg_buffer message;
    message.msg_type = 1;

    while (1) {
        //receive input user
        printf("Enter the command ('CREDS', 'AUTH: username password', 'TRANSFER filename'): ");
        fgets(input, sizeof(input), stdin);
        input[strcspn(input, "\n")] = 0;  // Remove the newline character if present

        if (strncmp(input, "AUTH:", 5) == 0) 
        {
            //if user auth, =1
            authenticated = 1;
            //copy input ke message
            strncpy(message.msg_text, input, sizeof(message.msg_text) - 1);
            msgsnd(msg_id, &message, sizeof(message), 0);
            printf("Command sent: %s\n", message.msg_text);
        } 

        else if (authenticated == 0 && strncmp(input, "TRANSFER ", 9) == 0) 
        {
            //if user auth =0, prevent transfer
            printf("You need to login first!!\n");
        } 
        
        else if (strncmp(input, "TRANSFER ", 9) == 0) 
        {
            //transfer file check
            const char* filename = input + 9;
            if (isFileTransferred(filename)) 
            {
                printf("File '%s' has already been transferred\n", filename);
            } else if (isFileInReceiver(filename)) 
            {
                printf("File '%s' already exists in the receiver folder\n", filename);
            } else 
            {
                //copy, send, add to transferred list
                strncpy(message.msg_text, input, sizeof(message.msg_text) - 1);
                msgsnd(msg_id, &message, sizeof(message), 0);
                addTransferredFile(filename);
                printf("Command sent: %s\n", message.msg_text);
            }
        } 
        
        else 
        {
            //copy input message, send ke receiver
            strncpy(message.msg_text, input, sizeof(message.msg_text) - 1);
            msgsnd(msg_id, &message, sizeof(message), 0);
            printf("Command sent: %s\n", message.msg_text);
        }
    }
    return 0;
}

```
a. Pendefinisian Struktur msg_buffer:
- Struktur msg_buffer digunakan untuk menyimpan pesan yang dikirimkan melalui sistem pesan. Ini memiliki dua anggota: msg_type (tipe pesan) dan msg_text (isi pesan). Tipe pesan digunakan untuk mengidentifikasi pesan.

b. Fungsi transfer_file:
- Fungsi ini menerima nama file sebagai argumen, membuka file sumber, membaca isinya, dan menyalinnya ke file tujuan. Ini juga menghitung ukuran file yang berhasil ditransfer.

c. Fungsi base64_decode:
- Fungsi ini digunakan untuk mendekode string yang telah dienkripsi dalam format Base64. Hasil dekripsi disimpan dalam bentuk byte yang diarahkan ke alamat yang ditentukan oleh pointer output.

d. Fungsi authenticate_user:
- Fungsi ini digunakan untuk memeriksa autentikasi pengguna. Ini membuka file "users.txt" yang berisi informasi pengguna (nama pengguna dan kata sandi yang dienkripsi dalam format Base64). Fungsi ini memeriksa apakah informasi otentikasi yang diberikan oleh pengguna sesuai dengan informasi yang ada dalam file "users.txt".

e. Fungsi main:
- Fungsi utama dari program. Ini berisi logika utama program yang melibatkan komunikasi melalui sistem pesan, autentikasi pengguna, dan transfer file.
- Program ini menggunakan kunci (key) yang dibuat dengan ftok untuk mendapatkan ID pesan dan mengakses sistem pesan.
- Program berjalan dalam loop tak terbatas (while (1)) dan menerima pesan dari pengirim melalui sistem pesan.
- Program mengenali beberapa jenis pesan, seperti "CREDS" untuk mengambil dan menampilkan informasi pengguna dari file "users.txt", "AUTH:" untuk melakukan autentikasi pengguna, dan "TRANSFER " untuk mentransfer file dari sender ke receiver.
- Jika pesan yang diterima tidak cocok dengan jenis pesan yang diharapkan, program akan mencetak "UNKNOWN COMMAND."
- Program juga menghapus antrian pesan (msg_id) saat selesai dijalankan.

### Hasil
---

1. Results dari receiver.c

![Results receiver.c](img/Soal_3/1.png)

2. Results dari sender.c

![Results sender.c](img/Soal_3/2.png)

### Kendala
---
1. Tidak ada kendala, tetapi salah konsep, yang dimana sudah ada users nya tetapi saya melakukan registrasi terlenih dahulu
### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Memperbaiki konsep dan menjalankan dengan lancar.

## Soal 4

### Study case soal 4
---
Takumi adalah seorang pekerja magang di perusahaan Evil Corp. Dia mendapatkan tugas untuk membuat sebuah public room chat menggunakan konsep socket. Ketentuan lebih lengkapnya adalah sebagai berikut:

### Problem
---
a. Client dan server terhubung melalui socket. 

b. Server berfungsi sebagai penerima pesan dari client dan hanya menampilkan pesan saja.   

c. karena resource Evil Corp sedang terbatas buatlah agar server hanya bisa membuat koneksi dengan 5 client saja.   

### Solution
---
[Source Code](./Soal_4)

1. Source Code Client.c.
```c
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define IP_PROTOCOL 0
#define IP_ADDRESS "127.0.0.1" // localhost
#define PORT_NO 15050
#define NET_BUF_SIZE 32
#define cipherKey 'S'
#define sendrecvflag 0

// function to clear buffer
void clearBuf(char* b)
{
  int i;
  for (i = 0; i < NET_BUF_SIZE; i++)
    b[i] = '\0';
}

// function for decryption
char Cipher(char ch)
{
  return ch ^ cipherKey;
}

// function to receive file
int recvFile(char* buf, int s)
{
  int i;
  char ch;
  for (i = 0; i < s; i++) {
    ch = buf[i];
    ch = Cipher(ch);
    if (ch == EOF)
      return 1;
    else
      printf("%c", ch);
  }
  return 0;
}

// driver code
int main()
{
  int sockfd, nBytes;
  struct sockaddr_in addr_con;
  int addrlen = sizeof(addr_con);
  addr_con.sin_family = AF_INET;
  addr_con.sin_port = htons(PORT_NO);
  addr_con.sin_addr.s_addr = inet_addr(IP_ADDRESS);
  char net_buf[NET_BUF_SIZE];
  FILE* fp;

  // socket()
  sockfd = socket(AF_INET, SOCK_DGRAM,
          IP_PROTOCOL);

  if (sockfd < 0)
    printf("\nfile descriptor not received!!\n");
  else
    printf("\nfile descriptor %d received\n", sockfd);

  while (1) {
    printf("\nPlease enter file name to receive:\n");
    scanf("%s", net_buf);
    sendto(sockfd, net_buf, NET_BUF_SIZE,
      sendrecvflag, (struct sockaddr*)&addr_con,
      addrlen);

    printf("\n---------Data Received---------\n");

  while (1) {
      // receive
      clearBuf(net_buf);
      nBytes = recvfrom(sockfd, net_buf, NET_BUF_SIZE,
              sendrecvflag, (struct sockaddr*)&addr_con,
              &addrlen);

      // process
      if (recvFile(net_buf, NET_BUF_SIZE)) {
        break;
   }
    }
    printf("\n-------------------------------\n");
  }
  return 0;
}
```
a. Pertama dimulai dengan membuat file client.c dan server.c 
Beberapa (includes), yang merupakan bagian dari program C untuk mengakses fungsi-fungsi yang digunakan dalam program ini. Header yang digunakan termasuk <arpa/inet.h>, <netinet/in.h>, <stdio.h>, <stdlib.h>, <string.h>, <sys/socket.h>, <sys/types.h>, dan <unistd.h>. Ini menyediakan fungsi-fungsi dan definisi yang diperlukan untuk mengelola soket, alamat jaringan, dan fungsi I/O.

b. Makro dan Konstanta: Beberapa konstanta dan makro didefinisikan, termasuk IP_PROTOCOL yang diatur ke 0 (menggunakan protokol default), IP_ADDRESS yang diatur ke "127.0.0.1" (localhost), PORT_NO yang diatur ke nomor port 15050, NET_BUF_SIZE yang diatur ke 32 (ukuran buffer), cipherKey yang digunakan untuk enkripsi sederhana, dan sendrecvflag yang diatur ke 0.

c. Fungsi clearBuf(): Ini adalah fungsi yang digunakan untuk membersihkan (mengisi dengan karakter null) buffer yang diberikan.

d. Fungsi Cipher(): Ini adalah fungsi yang digunakan untuk enkripsi sederhana dengan mengganti karakter dengan karakter XOR (eksklusif OR) dengan kunci cipherKey.

e.Fungsi recvFile(): Ini adalah fungsi yang digunakan untuk mendekripsi data yang diterima dari soket. Fungsi ini membaca karakter demi karakter, mendekripsi karakter dengan menggunakan fungsi Cipher(), dan mencetak karakter yang telah didekripsi ke layar. Fungsi ini mengembalikan 1 jika EOF (End of File) terdeteksi dan 0 jika masih ada data yang harus diterima.

Fungsi main(): Fungsi utama program, yang berisi logika utama dari program penerima file.

Pertama, beberapa variabel dideklarasikan, termasuk sockfd (file descriptor untuk soket), addr_con (struktur alamat), addrlen (panjang alamat), dan net_buf (buffer untuk menyimpan data yang diterima).

Selanjutnya, program menciptakan soket dengan memanggil socket() dengan parameter AF_INET (IPv4) dan SOCK_DGRAM (UDP). Hasilnya disimpan dalam sockfd. Jika pembuatan soket berhasil, program mencetak pesan bahwa "file descriptor" telah diterima.

Program memasuki loop tak terbatas (while (1)) yang meminta pengguna untuk memasukkan nama file yang akan diterima.

Nama file yang dimasukkan oleh pengguna dikirim ke server menggunakan sendto().

Kemudian program memasuki loop lain (while (1)) untuk menerima dan memproses data yang dikirimkan oleh pengirim.

Dalam loop tersebut, program menerima data dari pengirim menggunakan recvfrom(), kemudian mendekripsi data tersebut dengan fungsi recvFile(), dan mencetak data yang telah didekripsi ke layar.

Program akan terus berjalan hingga EOF (End of File) terdeteksi pada data yang diterima, dan kemudian kembali ke tahap meminta nama file baru untuk diterima.

Kode ini merupakan implementasi dasar untuk menerima file menggunakan protokol UDP dan melakukan enkripsi sederhana pada data yang diterima sebelum mencetaknya ke layar. Kode ini belum mencakup penanganan kesalahan dan validasi yang lengkap, sehingga sebaiknya digunakan hanya untuk tujuan pendidikan atau demonstrasi. 

2. Source Code Server.c.
```c
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define IP_PROTOCOL 0
#define PORT_NO 15050
#define NET_BUF_SIZE 32
#define cipherKey 'S'
#define sendrecvflag 0
#define nofile "File Not Found!"

// function to clear buffer
void clearBuf(char* b)
{
  int i;
  for (i = 0; i < NET_BUF_SIZE; i++)
    b[i] = '\0';
}

// function to encrypt
char Cipher(char ch)
{
  return ch ^ cipherKey;
}

// function sending file
int sendFile(FILE* fp, char* buf, int s)
{
  int i, len;
  if (fp == NULL) {
    strcpy(buf, nofile);
    len = strlen(nofile);
    buf[len] = EOF;
    for (i = 0; i <= len; i++)
      buf[i] = Cipher(buf[i]);
return 1;
  }

  char ch, ch2;
  for (i = 0; i < s; i++) {
    ch = fgetc(fp);
    ch2 = Cipher(ch);
    buf[i] = ch2;
    if (ch == EOF)
      return 1;
  }
  return 0;
}

// driver code
int main()
{
  int sockfd, nBytes;
  struct sockaddr_in addr_con;
  int addrlen = sizeof(addr_con);
  addr_con.sin_family = AF_INET;
  addr_con.sin_port = htons(PORT_NO);
  addr_con.sin_addr.s_addr = INADDR_ANY;
  char net_buf[NET_BUF_SIZE];
  FILE* fp;

// socket()
  sockfd = socket(AF_INET, SOCK_DGRAM, IP_PROTOCOL);

  if (sockfd < 0)
    printf("\nfile descriptor not received!!\n");
  else
    printf("\nfile descriptor %d received\n", sockfd);

  // bind()
  if (bind(sockfd, (struct sockaddr*)&addr_con, sizeof(addr_con)) == 0)
    printf("\nSuccessfully binded!\n");
  else
    printf("\nBinding Failed!\n");

  while (1) {
    printf("\nWaiting for file name...\n");

    // receive file name
clearBuf(net_buf);
    nBytes = recvfrom(sockfd, net_buf,
            NET_BUF_SIZE, sendrecvflag,
            (struct sockaddr*)&addr_con, &addrlen);

    fp = fopen(net_buf, "r");
    printf("\nFile Name Received: %s\n", net_buf);
    if (fp == NULL)
      printf("\nFile open failed!\n");
    else
      printf("\nFile Successfully opened!\n");

    while (1) {

      // process
      if (sendFile(fp, net_buf, NET_BUF_SIZE)) {
        sendto(sockfd, net_buf, NET_BUF_SIZE,
          sendrecvflag,
          (struct sockaddr*)&addr_con, addrlen);
        break;
   }
// send
      sendto(sockfd, net_buf, NET_BUF_SIZE,
        sendrecvflag,
        (struct sockaddr*)&addr_con, addrlen);
      clearBuf(net_buf);
    }
    if (fp != NULL)
      fclose(fp);
  }
  return 0;
}
```

a. Kemudian membuat source code untuk server.c yang digunakan untuk mengakses fungsi-fungsi yang dibutuhkan. Header yang digunakan termasuk <arpa/inet.h>, <netinet/in.h>, <stdio.h>, <stdlib.h>, <string.h>, <sys/socket.h>, <sys/types.h>, dan <unistd.h>. Ini menyediakan fungsi-fungsi dan definisi yang diperlukan untuk mengelola soket, alamat jaringan, dan I/O.

b. Makro dan Konstanta: Beberapa konstanta dan makro didefinisikan, termasuk IP_PROTOCOL yang diatur ke 0 (menggunakan protokol default), PORT_NO yang diatur ke nomor port 15050, NET_BUF_SIZE yang diatur ke 32 (ukuran buffer), cipherKey yang digunakan untuk enkripsi sederhana, sendrecvflag yang diatur ke 0, dan nofile yang berisi pesan "File Not Found!".

c. Fungsi clearBuf(): Ini adalah fungsi yang digunakan untuk membersihkan (mengisi dengan karakter null) buffer yang diberikan.

d. Fungsi Cipher(): Ini adalah fungsi yang digunakan untuk enkripsi sederhana dengan mengganti karakter dengan karakter XOR (eksklusif OR) dengan kunci cipherKey.

e. Fungsi sendFile(): Ini adalah fungsi yang digunakan untuk membaca file yang akan dikirim dan mengenkripsi data dari file tersebut. Fungsi ini membaca karakter demi karakter dari file, mendekripsi karakter menggunakan fungsi Cipher(), dan menyimpan karakter yang telah dienkripsi ke dalam buffer buf. Fungsi ini mengembalikan 1 jika EOF (End of File) terdeteksi pada file, dan 0 jika masih ada data yang harus dikirim.

f. Fungsi main(): Fungsi utama program, yang berisi logika utama dari program pengirim file.

g. Pertama, beberapa variabel dideklarasikan, termasuk sockfd (file descriptor untuk soket), addr_con (struktur alamat), addrlen (panjang alamat), dan net_buf (buffer untuk menyimpan data yang akan dikirim).

h. Program menciptakan soket dengan memanggil socket() dengan parameter AF_INET (IPv4) dan SOCK_DGRAM (UDP). Hasilnya disimpan dalam sockfd. Jika pembuatan soket berhasil, program mencetak pesan bahwa "file descriptor" telah diterima.

i. Program kemudian mencoba untuk melakukan bind() dengan alamat yang telah diatur sebelumnya. Ini mengikat soket ke alamat dan port yang ditentukan.

j. Program memasuki loop tak terbatas (while (1)) yang menunggu nama file yang akan dikirim dari pengguna. File name dikirimkan melalui soket ke penerima.

k. Setelah file name diterima, program membuka file tersebut dengan mode "r" (membaca). Jika file tidak ditemukan, program mencetak pesan kesalahan.

l. Program memasuki loop lain (while (1)) untuk mengirim data file yang telah dienkripsi ke penerima. Data file dikirim menggunakan sendto() ke alamat penerima.

m. Setelah seluruh file dikirim, program mencetak pesan bahwa pengiriman telah selesai dan menutup file yang dikirim.

n. Kode ini merupakan implementasi dasar untuk mengirim file menggunakan protokol UDP dan melakukan enkripsi sederhana pada data file sebelum mengirimkannya. Kode ini belum mencakup penanganan kesalahan dan validasi yang lengkap, sehingga sebaiknya digunakan hanya untuk tujuan pendidikan atau demonstrasi.

### Kendala
---
1. Tidak ada eror sama sekali, dan kendala sama sekali.

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Pada saat demo sudah lancar, dan tidak ada revisi sama sekali.

### Hasil
---

1. Hasil Run Server.c
![Results server.c](img/Soal_4/1.png)

2. Hasil Run Client.c
![Results client.c](img/Soal_4/2.png)

3. Hasil Run 5 client.
![Results 5 client](img/Soal_4/3.png)
